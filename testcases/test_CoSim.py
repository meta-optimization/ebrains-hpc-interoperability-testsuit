import pytest, sys, os, ast
import json, time, subprocess

# Version from Pip install
from pyunicoremanager.core import *

token = os.environ['my_token']
setup =  ast.literal_eval(os.environ["project_setup"])

def create_env(verbose=False):
    # Authentication for running jobs on the HPC system
    authentication = Authentication(token=token, access=Method.ACCESS_COLLAB, server=setup['server'])
    env = Environment_UNICORE(auth=authentication, env_from_user= setup)
    pym = PyUnicoreManager(environment=env, clean_job_storages=False, verbose=verbose)
    return pym

class TestCoSim():
    def test_project_access(self):       
        pym = create_env()
        job_steps = ["cd "+setup["abs_path"] , "ls -la"]
        job, result= pym.one_run(job_steps, setup["JobArguments"])

        assert len(result["stderr"]) == 0
        assert len(result["stdout"].split('\n')[1:-1]) > 1 #it keeps only the 1. 2.. and 3files
    
    def test_project_execution(self):        
        pym = create_env(verbose=False)
        job_steps = [ "cd "+ setup['abs_path'], setup["launcher"]]
        job, result= pym.one_run(job_steps, setup["JobArguments"])
        
        print(result["stderr"].split('\n'))
        print(result["stdout"].split('\n'))
        assert len(result["stderr"]) == 0
        
