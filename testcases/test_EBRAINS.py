import pytest, sys, os
import pyunicore.client as unicore_client
import json, time
import ebrains_drive

token = os.environ["my_token"]

class TestEBRAINS():
    def test_Drive_access(self):
        client = ebrains_drive.connect(token=token)
        all_repos = client.repos.list_repos()
        
        assert len(all_repos) >=0
        
        
