import pytest, sys, os, ast
# Version from Pip install
from pyunicoremanager.core import *

token = os.environ['my_token']
setup =  ast.literal_eval(os.environ["project_setup"])

def create_env(verbose=False):
    # Authentication for running jobs on the HPC system
    authentication = Authentication(token=token, access=Method.ACCESS_COLLAB, server=setup['server'])
    env = Environment_UNICORE(auth=authentication, env_from_user= setup)
    pym = PyUnicoreManager(environment=env, clean_job_storages=False, verbose=verbose)
    return pym

def create_test_file(name):
    filename = name
    with open(filename, "w") as f:
        f.write("this is some testdata for PyUnicoreManager")
    return filename

class TestPyUnicoreManager():
    def test_project_access(self):       
        pym = create_env()
        job_steps = ["cd $HOME" , "ls -la"]
        job, result= pym.one_run(job_steps, setup["JobArguments"])
        
        assert len(result["stderr"]) == 0
        assert len(result["stdout"].split('\n')[1:-1]) > 1 #it keeps only the 1. 2.. and 3files
    
    def test_endpoints_number(self):
        pym = create_env()
        
        assert len(pym.getStorage()) < 200 # reaching this number jobs endpoint will hidde the important ones: PROJECT, SCRATCH, HOME
        
    def test_getJobs(self):
        pym = create_env()
        list_jobs = pym.getJobs()
        
        assert list_jobs is not None
        assert len(list_jobs) >=0
    
    def test_cleanJobsEndpoints(self):
        pym = create_env()
        pym.clean_storages(endswith="-uspace") # Every Unicore job creates a Endpoint in a limited list of Endpoints.
        
        assert pym is not None
    
    def test_upload_file(self):
        pym = create_env()
        file="test_file.txt"
        file_localpath = create_test_file(name = "testcases/"+file)
        username = pym.client.access_info()['xlogin']['UID']
        
        filesToUpload = [[file_localpath, os.path.join(username,file)]] # [[A -> B],...]
        assert pym.uploadFiles(filesToUpload) == True
        
    def test_download_file(self):
        pym = create_env()
        file="test_file.txt"
        file_localpath = "testcases/"+file
        username = pym.client.access_info()['xlogin']['UID']
        
        filesToUpload = [[file_localpath, os.path.join(username,file)]] # [[B <- A],...]
        assert pym.uploadFiles(filesToUpload) == True
