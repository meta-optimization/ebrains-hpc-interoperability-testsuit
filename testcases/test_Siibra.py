import pytest, sys, os
import json, time
import matplotlib.pyplot as plt
import numpy as np
import siibra
#siibra.__version__
#import os
import warnings
warnings.filterwarnings('ignore', message='Unverified HTTPS request')

token = os.environ["my_token"]

def connectSiibra():
    # Pass the authentication to siibra
    try:
        with siibra.QUIET:
            siibra.set_ebrains_token(token)
        return siibra
    except:
        return None
class TestSiibra():    
    def test_Connection(self):
        # Pass the authentication to siibra
        siibra = connectSiibra()
        assert siibra is not None

    def test_Atlas(self):
        siibra = connectSiibra()
            
        atlas = siibra.atlases.MULTILEVEL_HUMAN_ATLAS
        julichmap = atlas.get_map(space='mni152', maptype='labelled')
        
        assert julichmap is not None
