import pytest, sys, os
import json, time
import ebrains_drive

token = os.environ["my_token"]

class TestBucket():
    
    def test_Bucket(self):
        bearer_token = token
        client = ebrains_drive.connect(token=bearer_token)

        try:
            all_repos = client.repos.list_repos()
        except Exception:
            print("Could not retrieve Repos from EBRAINS Drive!")
            all_repos = []
        dropdown_options = [(repo.name, repo) for repo in all_repos]            
            
        assert dropdown_options
