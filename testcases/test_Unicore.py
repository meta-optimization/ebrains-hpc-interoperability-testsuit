import pytest, sys, os
import pyunicore.client as unicore_client
import json, time

token = os.environ["my_token"]

#Note: at the moment only those server have queues
JSC_queues = {'JUWELS':['gpus','batch','mem192','develgpus','devel','develbooster'],
              'JUSUF': ["gpus", "batch", "develgpus"]}
    
cmd = "ls -la" #minimun of 2 rows "." and ".."    
    
    
def create_objects():
    if not token or len(token) <= 0:
        return None
    tr = unicore_client.Transport(token)
    registry_url = 'https://zam2125.zam.kfa-juelich.de:9112/HBP/rest/registries/default_registry'
    registry = unicore_client.Registry(tr,registry_url)
    return registry
    
def create_job():
    my_job = {}

    # executable / application
    my_job['Executable'] = "ls"

    # arguments - optional
    my_job['Arguments'] = ["-l"]
    
    return my_job
    
class TestUnicore():
    servers=["JUDAC","JURECA","JUWELS","JUSUF"]

    @pytest.mark.parametrize('server', servers)
    def test_Sites(self,server):
        registry = create_objects()
        dic = registry.site_urls
        assert server in dic.keys()
        
    @pytest.mark.parametrize('server', JSC_queues)
    def test_Sources(self, server):
        registry = create_objects()
        site = registry.site(server)
        server_info = site.access_info()

        assert len(JSC_queues[server]) == len(server_info['queues']['availableQueues'])
        assert all([a == b for a, b in zip(JSC_queues[server], server_info['queues']['availableQueues'])])
        
    @pytest.mark.parametrize('server', JSC_queues)
    def test_Jobs(self, server):
        registry = create_objects()
        site = registry.site(server)
        
        filename = "./testcases/test_file.txt"
        with open(filename, "w") as f:
            f.write("this is some testdata for Unicore")
        
        local_inputs = [filename]
        
        my_job = create_job()
        job = site.new_job(job_description=my_job, inputs=local_inputs)                
        time.sleep(2)
        assert job.status == unicore_client.JobStatus.READY, "Job is not running"
        
        seconds=0
        previous_state = unicore_client.JobStatus.READY
        while((job.status in [unicore_client.JobStatus.QUEUED,
                              unicore_client.JobStatus.RUNNING,
                              unicore_client.JobStatus.READY]) and seconds<=600):            
            time.sleep(1)
            if previous_state != job.status:#Avoid repeated
                previous_state = job.status
                print("in",seconds,"seconds:", job.status)
            seconds+=1
                    
            
        assert job.status in [unicore_client.JobStatus.READY, unicore_client.JobStatus.SUCCESSFUL]
